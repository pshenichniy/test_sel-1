def test_query_text_title_is_visible(browser):
    browser.get('https://google.com')
    query_window = browser.find_element_by_name('q')
    assert query_window.is_displayed()