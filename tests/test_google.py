
def test_query_window_is_visible(browser):
    browser.get('https://google.com')
    query_window = browser.find_element_by_name('q')
    assert query_window.is_displayed()

# def test_query_window_is_visible2(browser):
#     browser.get('https://google.com')
#     query_window = browser.find_element_by_name('g')
#     assert query_window.is_displayed()
